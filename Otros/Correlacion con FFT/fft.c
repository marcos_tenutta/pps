// Compilar con gcc y linkear : -lfftw3 -lm.

#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <fftw3.h>
#include "datos.h" 	


int debug = 0;

int encontrar_maximo(double*);
	
 
int main()
{
	int i= 0, desfase = 0, tolerancia = 1;
    double *channel0, *channel1, *out_inversa;
    fftw_complex *data0, *data1, *conv, *in_inversa;
    fftw_plan plan0, plan1, plan0_inversa;
       
    channel0 = (double*) fftw_malloc(sizeof(double) * N);
    channel1 = (double*) fftw_malloc(sizeof(double) * N);

    // Llenar vectores de entrada /////////////////////
    
    for (i=0;i<N;i++)
    {
		channel0[i] = vector[i];
	}
	
	    for (i=0;i<N;i++)
    {
		channel1[i] = vector2[N-i];  // Para que la convolución 
								    // sea una correlacion
	}

    //////////////////////////////////////////////////////////////
      
    data0 = (fftw_complex*) fftw_malloc (sizeof (fftw_complex) * (N/2+1));
    data1 = (fftw_complex*) fftw_malloc (sizeof (fftw_complex) * (N/2+1));
    conv  = (fftw_complex*) fftw_malloc (sizeof (fftw_complex) * (N/2+1));
        
    
    plan0 = fftw_plan_dft_r2c_1d(N, channel0, data0, FFTW_ESTIMATE);
    plan1 = fftw_plan_dft_r2c_1d(N, channel1, data1, FFTW_ESTIMATE);
    

    fftw_execute(plan0); /* repeat as needed */
    fftw_execute(plan1); /* repeat as needed */
    
    
    
    //  Normalizar escala     ////////////////////////////////////
    
    
    for (i=0;i<N/2+1;i++)
    
    {
		 data0[i] = creal(data0[i])/(N/2) + cimag(data0[i])/(N/2) * I;
	}

    for (i=0;i<N/2+1;i++)
    {
		 data1[i] = creal(data1[i])/N + (cimag(data1[i])/N) * I;
	}
	
	////////////////////////////////////////////////////////////////////

	//printf("Resultado FFT - (%d muestras - N/2+1, con N = %d)\n\n\n",N/2+1,N);

	//Llenar vector de entrada transformada inversa
	
	if (debug==1)
	{
		for (i=0;i<(N/2+1);i++)
		{
			printf("Muestra %d:\t%.2f\t%.2f\n",i,creal(data0[i]),cimag(data0[i]));
		}
	}


	// Respuesta en frecuencia - módulo
	
		if (debug==1)
	{
		
	printf ("\n");
	printf("Espectro de frecuencias\n\n");
	
	for (i=0;i<(N/2+1);i++)
    {
		printf("Muestra %d:\t%.2f\n",i,cabs( data0[i]) );
	}

	printf ("\n\n");
	
	}
	
	// Producto de complejos - Convoulición

	
    for (i=0;i<(N/2+1);i++)
    {
	    conv[i]	= data0[i] * data1[i];
	}


	//Convolución en dominio de la frecuencia
	
	if (debug==1)
	{
		printf("Convolución en el dominio de la frecuencia...\n\n");	
		
	
		for (i=0;i<(N/2+1);i++)
		{
			printf("Muestra %d:\t%.2f\t%.2f\n",i,creal(conv[i]),cimag(conv[i]));
		}
	}
	
    // FFTW-1

    out_inversa = (double*) fftw_malloc (sizeof(double) * 2*(N/2+1));
    in_inversa = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N/2+1));
    
	//Llenar vector de entrada transformada inversa

    for (i=0;i<N/2+1;i++)
    {
		 in_inversa[i] = creal(conv[i]) + cimag(conv[i]) * I;
	}
    
        
    plan0_inversa = fftw_plan_dft_c2r_1d(2*(N/2+1),in_inversa,out_inversa, FFTW_ESTIMATE);
									

	fftw_execute(plan0_inversa);
	

	if (debug==1)
	{
		printf("\n\nConvolución en el dominio del tiempo...\n\n");	
    

		for (i=0;i<2*(N/2+1);i++)
		{
			printf("Muestra %d:\t%.2f\n",i,out_inversa[i]);
			}
			
	}
	
	desfase = encontrar_maximo(out_inversa);
	
	if (debug==1)
	{
		printf ("\tDesfase calculado: %d\t", desfase);
		printf ("Desfase simulado: %d\t", desplazamiento);
	}
	

	if ( abs (abs(desplazamiento)- abs(desfase)) < tolerancia)
	{
		printf("OK\n");
		//printf("----------   Test Passed!   ----------\n");
		}
	else
	{
		printf("----------   Test Failed!   ----------   ");
		printf ("\tDesfase calculado: %d\t", desfase);
		printf ("Desfase simulado: %d\n", desplazamiento);
		}
		    
    fftw_destroy_plan(plan0);
    fftw_destroy_plan(plan1);
    fftw_destroy_plan(plan0_inversa);
    
    fftw_free(channel0);
    fftw_free(channel1);
    fftw_free(data0);
    fftw_free(data1);
    fftw_free(in_inversa);
    fftw_free(out_inversa);
	fftw_free(conv);
    fftw_cleanup();
    





  
	return 0;
 }
 
 
 
 int encontrar_maximo(double* vector)
{
	int i=0, pos=0;
	double mayor = 0;
			
    for ( i=0; i< (N/2+1) ; i++)
    {

        if (mayor < vector[i]) 	
        {							  
			mayor = vector[i];
            pos = i;
        }
     }
       
    return pos;
	}
 
 
// USAR VECTORES DE TAMAÑO POTENCIA DE DOS grandes. (512,1024)

// El vector que devuelve va de 0 (continua) a N/2; 

// Resolución de la DFT:
// 48000 / 32
// f = (0: 1/32: 32)*48000



// En complex.h:

// cabs(double complex); Calcular valor absoluto de cun complejo tipo double.
// Para el producto existe la sobrecarga de operadores, así que todo bien jeje.
// Si los operandos de * son complex (float, double, etc) pueden multiplicares directamente,
// y el resultado debe guradarse en otro complejo igual.



//			FFT - FFTW-1

// Input and output arrays are of different sizes and types: the input 
// is n real numbers, while the output is n/2+1 complex numbers 
// this also requires slight “padding” of the input array.

// The inverse transform (complex to real) has the side-effect of 
// overwriting its input array, by default.

//----------------------------------------------------------------------

// fftw_plan fftw_plan_dft_r2c_1d(int n, double *in, fftw_complex *out,
                               //unsigned flags);
// fftw_plan fftw_plan_dft_c2r_1d(int n, fftw_complex *in, double *out,
                               //unsigned flags);

// n is the “logical” size of the DFT, not necessarily the physical size
// of the array. In particular, the real (double) array has n elements,
// while the complex (fftw_complex) array has n/2+1 elements 
// (where the division is rounded down).

// As noted above, the c2r transform destroys its input array even for 
// out-of-place transforms. This can be prevented, if necessary, 
// by including FFTW_PRESERVE_INPUT in the flags, with unfortunately
// some sacrifice in performance
