##------------------------------------------------------------------------------
## DEFINICION DE VARIABLES GLOBALES
##------------------------------------------------------------------------------

filename = "datos.h";          # Donde se guarda el vector.

signal_noise_ratio = 0.1;      # Se indica en veces, no en dB. Ver DOC al final.

graficar = 0;

tam = 64;                            # Tamaño del vector.
f = 4500;                            # Frecuencia de la señal simulada.
fs = 48000;                          # Frecuencia de muestreo

arg = argv();                       # Parámetros desde la terminal de comandos.
desplazamiento = str2num(arg{1});   # Delay en muestras entre los vectores.

n = [0:1:tam + desplazamiento - 1];

y = sin (2*pi * f/fs * n );

y = round(10000 * y) / 10000;

## Elegir Ruido - Normal o Univariable.

## Distribución normal
noise = randn(1,tam+desplazamiento) * signal_noise_ratio;

##Distribucion Univariable
## noise = rand  (1,tam + desplazamiento)*2*signal_noise_ratio - signal_noise_ratio;

noise = round(10000 * noise) / 10000;

y = y + noise;

channel0 = y(1:tam);
channel1 = y(desplazamiento+1:tam+desplazamiento);

k = 1:1:tam;

if graficar== 1
  hold on;
  xlabel ("Muestras: n");
  ylabel ("Channels");
  title ("Simulacion de vectores de entrada");
  axis ([0, tam, -2, 2]);  
  plot (k , channel0)
  hold on;
  plot (k , channel1)
  print -djpg simulacion.jpg;
end

## Crear vectores para copiar a programa en C.

function res = export_vector (vector_name, vector, vector_size)
  
  res = ["double ", vector_name,'[', num2str(vector_size),'] = { '];
  
  for j=1:vector_size-1
    res = [res, num2str(vector(j)), ", "];
  end
  res = [res, num2str(vector(vector_size)), "};\n"];
  
  endfunction


vector0 = export_vector("vector",channel0,tam);
vector1 = export_vector("vector2",channel1,tam);

## Generar las demás lineas del archivo.

include_data=["#ifndef DATOS_H\n","#define DATOS_H\n"];
tam_data = ["#define N ",num2str(tam),"\t\t//Size of channels\n"];
shift_data = ["int desplazamiento = ",num2str(desplazamiento),";\n"];
include_end_data=["#endif /* DATOS_H */"];

## Escribir vector a un archivo con fputs:

fid = fopen (filename, "w");
fputs (fid, include_data);
fputs (fid, tam_data);
fputs (fid, shift_data);
fputs (fid, vector0);
fputs (fid, vector1);
fputs (fid, include_end_data);
fclose (fid);

#fprintf("Archivo generado correctamente.");