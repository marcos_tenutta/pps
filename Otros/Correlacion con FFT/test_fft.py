#!/usr/bin/python3
# -*-coding: utf-8-*-

import subprocess

def simulacion_test(desplazamiento):

	subprocess.call(["octave","simulacion_fft.m", str(desplazamiento)])     
	subprocess.call(["gcc","-Wall","-o","fft","fft.c","-lfftw3","-lm"])
	subprocess.call(["./fft"])
	return 0


#lista_desplazamientos = [1,3]

lista_desplazamientos = []

for i in range(0,32):
	lista_desplazamientos.append(i)

print ("Se van a simular",len(lista_desplazamientos),"casos:",lista_desplazamientos)

# for repeticion in range(1,15):
#	print ("--------------------------------------------------------------------------------------------------------")

for desplazamiento in lista_desplazamientos:
		simulacion_test(desplazamiento)







 # En la llamada a octave se pasa el parámetro a octave para la simulacion con un desplazamiento determinado.
