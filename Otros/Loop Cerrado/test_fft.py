#!/usr/bin/python3
# -*-coding: utf-8-*-

import subprocess, sys	# Sys solo es necesario para hacer flush pero no es indispensable
import ast				# Convierte variables recibida de los procesos como texto, en variables de python (int,str,listas)

from matplotlib import *	# Librerías para generar imágenes
from numpy import *
from pylab import *

def simulacion_test(desplazamiento):

	subprocess.run(["octave","simulacion_fft.m", str(desplazamiento)])     
	subprocess.run(["gcc","-Wall","-o","fft","fft.c","-lfftw3","-lm"])
	serialized_text_data = subprocess.run(["./fft"],text=True, check=True, stdout=subprocess.PIPE).stdout  #stderr = log.txt
	realimentacion = ast.literal_eval(serialized_text_data)
	return realimentacion


lista_desplazamientos = [31, 15, 4, 1, 27]

#lista_desplazamientos = []

#for i in range(0,32):
#	lista_desplazamientos.append(i)

print ("Se van a simular",len(lista_desplazamientos),"casos:",lista_desplazamientos)

# for repeticion in range(1,15):
#	print ("--------------------------------------------------------------------------------------------------------")

iteraciones = 10  # Cuantas veces se realimenta para cada caso a simular
lista_resultados_simulaciones = []

for desplazamiento in lista_desplazamientos:

	puntos_grafica = []
	puntos_grafica.append(desplazamiento)

	for i in range(iteraciones):

		print (desplazamiento, end=" ")
		realimentacion = simulacion_test(desplazamiento)
		desplazamiento += realimentacion
		puntos_grafica.append(desplazamiento)
		sys.stdout.flush()

	print ()
	lista_resultados_simulaciones.append(puntos_grafica)

title = u'Simulación de desplazamiento con realimentación'
plt.figure(1)
plt.title(title)
plt.xlabel('Iteracion (n)')
plt.ylabel('Desplazamiento (m)')
grid()

cantidad_muestras = list (range(1,iteraciones+2))

i=0

for simulacion in lista_resultados_simulaciones:
	print (simulacion)
	plot(cantidad_muestras,simulacion,'-o',label='Sim '+str(i+1))
	plt.legend(loc='upper right')
	i+=1

plt.savefig('Simulacion'+''+'.png')
show()






 # En la llamada a octave se pasa el parámetro a octave para la simulacion con un desplazamiento determinado.
