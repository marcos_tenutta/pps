**Crear proyecto en VIVADO y exportar hardware a VITIS**

Se utilizan los dos pdfs que se encuentran en la carpeta:

    - The_Zynq_Book_Tutorials_Aug_15(1).pdf
    - ug1165-zynq-embedded-design-tutorial.pdf

Teniendo instalado Vivado, seguir los pasos del ejemplo 1 del tutorial "The Zynq Book Tutorial" (Es del año 2015 así que puede puede presentar diferencias muy sutiles en el aspecto gráfico, pero los pasos son los mismos, de haber diferencias son muy pequeñas). Las diferencias ocurren cuando se termina de generar el proyecto en vivado (al completar el paso de generar bit stream) y se tiene que exportar el hardware descripto a Vitis para configurar la fpga y programar luego el micro embebido (antes se usaba SDK, que es lo que explica el tutorial). 


Se crea un proyeto y se elige la placa Board: "ZedBoard Evaluacion and Development Kit", Rev: "C".

Continuar con los pasos del tutorial...

Nota: Al agregar los IP del zynq, me salieron los siguientes warnings:

    WARNING: [BD 41-176] The physical port 'S_AXI_GP2_rd_socket' specified in the portmap, is not found on the block! 
    WARNING: [BD 41-176] The physical port 'S_AXI_GP2_wr_socket' specified in the portmap, is not found on the block! 
    WARNING: [BD 41-176] The physical port 'S_AXI_GP3_rd_socket' specified in the portmap, is not found on the block! 
    WARNING: [BD 41-176] The physical port 'S_AXI_GP3_wr_socket' specified in the portmap, is not found on the block! 

Pero no me hicieron problemas para completar el tutorial.


Cuando se quiere Generar Bit Stream, Vivado brinda la opcióón de sintetizar el diseño en la computadora local o en una remota, y la cantidad de jobs que queremos usar para el proceso. Se elige la compu local con 2 jobs (opciones por defecto). Puede tardar varios minutos en completar la síntesis. Continuar con los pasos y exportar el hardware diseñado al directorio deseado.

**Continuación en Vitis**

Para continuar el proceso seguimos los pasos de la guía "ug1165-zynq-embedded-design-tutorial.pdf", página 22, **Exporting Hardware to the Vitis Software Platform**, si bien carga un proyecto diferente al que veníamos haciendo, nos sirve para ver cómo es el proceso.

1) Primero se crea una "Plataforma de Hardware" seleccionando  la opción de usar como base el proyecto exportado en vivado (.xsa), 
se elige la ruta del .xsa y luego se marca como Sistema Opertivo: "Standalone", y como Procesador: ps7_cortex_a9_0. Dejar marcada la opción de "Generate Boot Components". Seguir hasta el paso 13, con el que finaliza la configuración del la generación de la plataforma de hardware.

2) El paso anterior es similar a abrir el hardware exportado con SDK en el tutorial que veníamos haciendo en The_Zynq_Book_Tutorial, más precisamente el principio del ejemplo 1-C.
Luego hay que crear el "**Proyecto de Aplicación**", que es muy similar a cómo se crea en los pasos del ejemplo 1C de "The Zynq tutorial", pero tenemos el paso extra de **seleccionar la plataforma de hardware creada anteriormente en el punto (1) de esta guía**. Entonces continuaremos la creación del proyecto de aplicación guiándonos por "The_Zynq_Book_Tutorial" por el ejemplo 1C hasta el punto "j" inclusive donde se configura la FPGA. Finalmente, se sigue en los puntos 9,10,11,12,13 de la página 31 y 32 de la guía "ug1165-zynq-embedded-design-tutorial.pdf" para poder buildear, compilar y cargar la aplicación a la fpga ya configurada. [Tener en cuenta que en el paso 9, "buildea" la aplicación llamada hello_world, y para nuestro caso se llama LED_test (o el nombre que hayas elegido para la App.) ]
.




