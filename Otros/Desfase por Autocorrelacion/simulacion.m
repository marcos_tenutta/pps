##------------------------------------------------------------------------------
## DEFINICION DE VARIABLES GLOBALES
##------------------------------------------------------------------------------

filename = "datos.h";          # Donde se guarda el vector.
graficar = 0;                  # Bandera para evitar mostrar todas las gráficas.

signal_noise_ratio = 0.2;      # Se indica en veces, no en dB. Ver DOC al final.

tam = 1000;                    # Tamaño del vector.
f = 1000;                      # Frecuencia de la señal simulada.
fs = 48000;                    # Frecuencia de muestreo



arg = argv();                       # Parámetros desde la terminal de comandos.
desplazamiento = str2num(arg{1});   # Delay en muestras entre los vectores.

#  Se define el desplazamiento si no se recibe por parámetro.
#  Comentar al llamarlo desde el archivo de python.
#
#  desplazamiento = 5;

n = [0:1:tam + desplazamiento - 1];

y = sin (2*pi * f/fs * n );

y = round(10000 * y) / 10000;

## Elegir Ruido - Normal o Univariable.

## Distribución normal
noise = randn (1,tam+desplazamiento)*signal_noise_ratio;

##Distribucion Univariable
## noise = rand  (1,tam + desplazamiento)*2*signal_noise_ratio - signal_noise_ratio;

noise = round(10000 * noise) / 10000;

y = y + noise;

##  if graficar== 1 
##    figure
##    stem (n , y)
##  end

channel0 = y(1:tam);
channel1 = y(desplazamiento+1:tam+desplazamiento);

## Forma 2 de generar señal: (No es muy realista).
## n = [0:1:tam- 1];
## channel1 = [ y(1+desplazamiento:tam) , zeros(1,desplazamiento) ];

k = 1:1:tam;


if graficar== 1
  hold on;
  xlabel ("Muestras: n");
  ylabel ("Channels");
  title ("Simulacion de vectores de entrada");
  axis ([0, 200, -2, 2]);  
  plot (k , channel0)
  hold on;
  plot (k , channel1)
  print -djpg simulacion.jpg;
end



##------------------------------------------------------------------------------
## COMPROBACIONES
##------------------------------------------------------------------------------
##  tam
##  desplazamiento
##  disp("Tamaño del vector más desplazamiento: ")
##  length(n)
##  disp("Tamaño del canal 0:")
##  length(channel0)
##  disp("Tamaño del canal 1:")
##  length(channel1)
##
##------------------------------------------------------------------------------
## Medio ciclo son 24 muestras, y(25) = 0.
##
## Guia para los subindices, el ciclo dividido en cuartos.
##
##  y(1)  - y(12).
##  y(13) - y(24).
##  y(25) - y(36).
##  y(37) - y(48).
##
##  channel0(1)
##  channel0(13)
##  channel0(25)
##  channel0(37)
##
##  stem(k,channel0+channel1)
##------------------------------------------------------------------------------



## Crear vectores para copiar a programa en C.

function res = export_vector (vector_name, vector, vector_size)
  
  res = ["float ", vector_name,'[', num2str(vector_size),'] = { '];
  
  for j=1:vector_size-1
    res = [res, num2str(vector(j)), ", "];
  end
  res = [res, num2str(vector(vector_size)), "};\n"];
  
  endfunction


vector0 = export_vector("channel0",channel0,tam);
vector1 = export_vector("channel1",channel1,tam);

## Generar las demás lineas del archivo.

include_data=["#ifndef DATOS_H\n","#define DATOS_H\n"];
tam_data = ["#define N ",num2str(tam),"\t\t//Size of channels\n"];
define_data = "#define M 2*N - 1\t//Size of correlation\n";
shift_data = ["int desplazamiento = ",num2str(desplazamiento),";\n"];
include_end_data=["#endif /* DATOS_H */"];

## Escribir vector a un archivo con fputs:

fid = fopen (filename, "w");
fputs (fid, include_data);
fputs (fid, tam_data);
fputs (fid, define_data);
fputs (fid, shift_data);
fputs (fid, vector0);
fputs (fid, vector1);
fputs (fid, include_end_data);
fclose (fid);

fprintf("Archivo generado correctamente.");

################################################################################
##################          DOCUMENTACION            ###########################
################################################################################
##
##------------------------------------------------------------------------------
## CREACIÓN DE FUNCION SENOIDAL MUESTREADA
##------------------------------------------------------------------------------
## y = sen (0)
## y = sen (wt)
## y= sen ((2*pi*f_señal)*t);
##
## Teniendo en cuenta que t = n*Ts, y que Ts = 1/ fs, entonces:
##
##
##                 y = sen (2*pi*f_señal/fs * n)
##
##
## Para una señal de 1Khz, muestreada con fs=48000 Khz, ¨ts = 20.83 [us].
##
## y = sin (2*pi * 1000/48000 * n ).
##
## Donde n=0,1 ,2 ,...  Con n=48000 tendría 1 segundo, y 
## cada 48 muestras 1 ciclo. Tomando un vector de 1000 muestras
## tengo 20.83 ciclos.
##
##------------------------------------------------------------------------------
## IMPRIMIR CON FORMATO
## -----------------------------------------------------------------------------
## sprintf("%.2f", 32.073) -> Esta sólo formatea pero no cambia el valor.
##
## for i=1:48
##   disp(sprintf("%.3f", y(i)))
##   i++;
## endfor
##------------------------------------------------------------------------------
## REDONDEO
##------------------------------------------------------------------------------
## Para redondear el vector:
##
## res = round(100 * 37.073) / 100 = 37.07
##
## -----------------------------------------------------------------------------
## CONTROL DE FLUJO DE PROGRAMA
## -----------------------------------------------------------------------------
##  switch var
##      case 29
##    disp("a");
##      case 57
##    disp("b");
##      otherwise
##    disp("wtf...no hay chance");
##    disp(var);
##  end
##------------------------------------------------------------------------------
##  i=0;
##  while i<3
##      ++i
##  end
##------------------------------------------------------------------------------
##  for i=0:2
##      ++i
##  end
##------------------------------------------------------------------------------
##  if var == valor
##    body
##  elseif
##    body
##  else
##    body
##  end
################################################################################
##  Generación de imgenes con plots de Octave
################################################################################
##  n=0:1:30;
##  x=0:0.033:1;
##  y = sin(2*pi*n/30);
##  stem (n,y);
##  hold on;
##  stem (n,x);
##  xlabel ("Muestras: n");
##  ylabel ("Y[n]");
##  title ("Simulacion");
##  axis ([0, 30, -1.1, 1.1]);
##  text (0.65, 0.6175, ['']);
##  print -djpg simulacion.jpg;
##  hold off;
##  stem (n,x);
##  print -djpg simulacion2.jpg;
################################################################################
## GENERACION DE RUIDO - SEÑAL ALEATORIA
################################################################################
##  Se utiliza la función randn(). Tiene una distribución normal con
##  valor medio 0, y varianza 1.
##
##  Al generar la señal se multiplica por el coeficiente de señal ruido
##  modificando la varianza en dicho valor. Lo que no quiere decir que haya
##  valores menos problables que sean picos.