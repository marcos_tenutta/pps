#include "datos.h" 

int encontrar_maximo(float*);
void input_side_conv (float*,float*,float*);
void output_side_conv(float*,float*,float*);


void input_side_conv(float *x, float *h, float *y)
{
    int i,j;
    for(i=0;i<N; i++)
        for(j=0;j<N;j++)
            y[i+j]=y[i+j]+x[i]*h[j];
}
 
void output_side_conv(float *x, float *h, float *y)
{
    int i,j;
    for(i=0;i<M; i++) {
        y[i]=0;
        for(j=0;j<N;j++) {
            if(i-j<0 || i-j>=N) continue;
            y[i] = y[i]+h[j]*x[i-j];
        }
    }
}

int encontrar_maximo(float* funcion)
{
	int i=0, pos=0;
	float mayor = 0;
			
    for ( i=0; i< M; i++)
    {

        if (mayor < funcion[i]) 	  // Como es una autocorrelación al fin y al cabo, 
        {							  // no es necesario el valor absoluto, porque obtenemos x²(t)
			mayor = funcion[i];
            pos = i;
        }
     }
       
    return pos;
	}
