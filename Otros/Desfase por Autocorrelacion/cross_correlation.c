#include <stdio.h>
#include <stdlib.h>            // abs()
#include "datos.h" 			   //Generado con script de Octave.
#include "funciones.h"

int debug = 0;  // Para debug, imprime la correlacion, las posiciones de
				// la autocorrelación y la correlación cruzada.


int tolerancia = 5;	   // Elegir el margen de tolerancia para determinar
					   // si es o no exitosa la simulación.
					

//  El desfase calculado debe encontrarse en el intervalo 
// (desplazamiento_real - tolerancia, desplazamiento real + tolerancia)  

int main()
{

    float aux[N] = {}; 		   // Auxiliar para  invertir un canal v[-n]
    
    float y1_correlacion[M] = {};
    float y2_correlacion[M] = {};
    int i=0, posicion_max_correlacion=0, desfase=0;

	
	//Correlación => v1[n] * v2[-n]
	
	for (i=0;i<N;i++)					 
	{
		aux[i] = *(channel0+(N-1)-i) ;   //aux[n] = channel0[-n]
		}

    input_side_conv(channel1, aux, y1_correlacion);
    
    // output_side_conv(channel1, aux, y2_correlacion);
	
	if (debug == 1)
	{
		printf("Pos\tInput Side\tOutput Side\n\n");
		for(i=0;i<M;i++)
			{
				printf("%d)\t[%2f]\t\t[%2f]\n", i,y1_correlacion[i], y2_correlacion[i]);
				}
		}
			
	//Se busca el máximo
	
	posicion_max_correlacion = encontrar_maximo(y1_correlacion);
	
	desfase = posicion_max_correlacion - (N-1);
	
	printf ("\tDesfase calculado: %d\t", desfase);
	printf ("Desfase simulado: %d\t", desplazamiento*-1);
	
	if ( abs (abs(desplazamiento)- abs(desfase)) < tolerancia)
		printf("----------   Test Passed!   ----------\n");
	else
		printf("----------   Test Failed!   ----------\n");
	
	if (debug == 1)
	{
		printf ("\nPos Max. Autocorrelacion: %d\t", N-1);
		printf ("Pos Max. Correlacion: %d\t", posicion_max_correlacion);			
		}

    return 0;
}

	////////////////////////////////////////////////////////////////////
    // El desfase máximo sin ruido esté dado por la cantidad de       //
	// muestras por ciclo. Con 50 muestras por ciclo, el método       //
	// funciona hasta un desplazamiento desde 0 a 24.		  		  //
	////////////////////////////////////////////////////////////////////
	
	//	Calculo del desfase, sería la diferencia entre el max de la 
	//	correlación y la autocorrelación (segun yo, jeje).
	//	La Autocorrelacion tiene máximo en la pasada numero N  
	//	(segun yo jaja, sería lo lógico), esa porición en el vector
	// 	corresponde a la posicion N - 1.
