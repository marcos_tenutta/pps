clc
clear

N = 1000;
s_r = 0.2;
n=[0:N-1];

y=sin(2*pi*n/100);

ruido_dist_univar = rand  (1,1000)*2*s_r-s_r;
ruido_dist_normal = randn (1,1000)*s_r;

canal_ruido_univar = y + ruido_dist_univar;
canal_ruido_normal = y + ruido_dist_normal;

hold on;
title ("Señal limpia de una frecuencia");
axis ([0, 1000, -3, 3]);
plot(n,y);
figure
hold on;
title ("Ruido Distribución Univariante");
axis ([0, 1000, -2, 2]);
text (100, 1, ['M A X :',num2str(max(ruido_dist_univar)),"\n\n",'M I N: ',num2str(min(ruido_dist_univar))]);
plot(ruido_dist_univar);
figure
hold on;
title ("Ruido Distribución Normal");
axis ([0, 1000, -2, 2]);
text (100, 1.8, ['M A X :',num2str(max(ruido_dist_normal)),"        ",'M I N: ',num2str(min(ruido_dist_normal))]);
text (100, 2.5, ['hola','perro']);
plot(ruido_dist_normal);
figure
hold on;
title ("Señal final con Ruido Univariante");
axis ([0, 1000, -3, 3]);
text (100, 2, ['M A X :',num2str(max(canal_ruido_univar)),"\n\n",'M I N: ',num2str(min(canal_ruido_univar))]);
plot(canal_ruido_univar);
figure
hold on;
title ("Señal final con Ruido Normal");
axis ([0, 1000, -3, 3]);
text (100, 2.5, ['M A X :',num2str(max(canal_ruido_normal)),"        ",'M I N: ',num2str(min(canal_ruido_normal))]);
plot(canal_ruido_normal);


disp("Maximos y Minimos del nivel de ruido univariante:");
disp(max(ruido_dist_univar));
disp(min(ruido_dist_univar));
disp("Maximos y Minimos del nivel de ruido normal:");
disp(max(ruido_dist_normal));
disp(min(ruido_dist_normal));
disp("Maximos y Minimos del canal con ruido univariante:");
disp(max(canal_ruido_univar));
disp(min(canal_ruido_univar));
disp("Maximos y Minimos del canal con ruido normal:");
disp(max(canal_ruido_normal));
disp(min(canal_ruido_normal));
