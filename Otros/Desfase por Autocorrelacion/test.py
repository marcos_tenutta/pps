#!/usr/bin/python3
# -*-coding: utf-8-*-

import subprocess

def simulacion_test(desplazamiento):

	subprocess.call(["octave","simulacion.m", str(desplazamiento)])     
	subprocess.call(["gcc","-Wall","-o","cross_correlation","cross_correlation.c"])
	subprocess.call(["./cross_correlation"])
	return 0


lista_desplazamientos = [1,20,55,72,81,90,100]

#lista_desplazamientos = []

# for i in range(0,90):
# 	lista_desplazamientos.append(i)

print ("Se van a simular",len(lista_desplazamientos),"casos:",lista_desplazamientos)

# for repeticion in range(1,15):
#	print ("--------------------------------------------------------------------------------------------------------")

for desplazamiento in lista_desplazamientos:
		simulacion_test(desplazamiento)







 # En la llamada a octave se pasa el parámetro a octave para la simulacion con un desplazamiento determinado.