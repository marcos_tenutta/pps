# Algoritmos para detectar el desfase entre señales #

![](https://selecciones-1bf98.kxcdn.com/wp-content/uploads/2015/03/que-son-las-ondas-y-cuales-son-sus-caracterisiticas-700x408.jpg)

# Wiki del proyecto - enlacés útiles: 
https://gitlab.com/marcos_tenutta/pps/-/wikis/Enlaces-de-interés

## Requerimientos

- python3
- octave
- módulos de python necesarios: numpy, matplotlib

Sino se tienen las librerías anteriores, es necesario Pip:

- pip3


### Instalación de pip3 en caso de no tener las librerías instaladas
```
sudo apt-get install pip3
```

### Para instalar las librerias numpy y matplotlib de python se debe utilizar pip3:


```
sudo pip3 install matplotlib, numpy
```

Nota: Debe tenerse instalado pip3 para poder realizar el paso anterior.


## Configurar el usuario de git con la cuenta de gitlab, y agregar el repo remoto: ##

```
git config --global user.name "Tu Nombre"
git config --global user.email "tumail@ejemplo.com"
git remote add origin git@gitlab.com:marcos_tenutta/pps.git
```

## Generar Simulación ##

Para correr la simulacion se ejecuta el archivo de python "test.py" o "fft_simulacion.py" en la carpeta correspondiente:

```
./simulacion.py
```


El archivo debe tener permiso de ejecucion, si no lo tiene, agregarlo:

```
chmod +x test.py
```

Esto hará 3 cosas:

1) En primer lugar generará los datos de la simulación a través de un script realizado en OCTAVE, los cuales se escriben en "datos.h".
2) En segundo lugar se compila y ejecuta un programa en C con los nuevos datos, llamado fft.c
3) Se ejecuta el programa fft para calcular el desfase.
4) Se muestra cómo se corrige la posicion simulada del robot
5) Se muestra una gráfica final de los resultados, los cuales son exportados a archivos csv y excel

## Git Workflow: ##
```
git pull
git checkout -b new_branch_name
git status
git add
git commit -m "mensaje"

git push origin new_branchname
git push origin master

```

## Informe: ##
https://drive.google.com/file/d/144gr2Ti-Oi6d8TQ2mUC3sOhdCISEY1HB/view?usp=sharing
