#!/usr/bin/python3
# -*-coding: utf-8-*-

import subprocess, sys	# Sys solo es necesario para hacer flush pero no es indispensable
import ast				# Convierte variables recibida de los procesos como texto, en variables de python (int,str,listas)

from matplotlib import *	# Librerías para generar imágenes
from numpy import *
from pylab import *


generador_de_canales = "generar_canales_de_audio.m"
post_procesamiento	 = "procesamiento_datos.m"

archivo_resultados = 'resultados.csv'

cantidad_columnas_csv = 6
columna_correcion_desfase = 3	# Columna correspondiente a la corección del desfase en el csv devuelto por fft.c

with open(archivo_resultados, 'w') as f:
	f.write("Simulación, Iteración, Desplazamiento Simulado, Desplazamiento Calculado, Posición, Corrección Desfase,\n")


def simulacion_test(desplazamiento):

	subprocess.run(["octave",generador_de_canales, str(desplazamiento)])     
	subprocess.run(["gcc","-Wall","-o","fft","fft.c","-lfftw3","-lm"])
	serialized_text_data = subprocess.run(["./fft"], check=True, stdout=subprocess.PIPE).stdout  #stderr = log.txt
	serialized_text_data = serialized_text_data.decode()
	serialized_text_data = serialized_text_data+'\n'
	with open(archivo_resultados, 'a') as f:
		f.write(serialized_text_data)
	serialized_text_data = list(  ast.literal_eval(serialized_text_data))
	realimentacion = serialized_text_data[ columna_correcion_desfase - 1 ]
	return realimentacion


# Forma 1: Simular casos discretos, agregarlos a la lista:
lista_desplazamientos = [31, 15, 4, 1, 27]

# Forma 2: Simular un rango de valores entre dos extremos definidos:
# for i in range(0,32):
#	 lista_desplazamientos.append(i)

print ("Se van a simular",len(lista_desplazamientos),"casos:",lista_desplazamientos)

nro_simulacion=0
iteraciones = 10
lista_resultados_simulaciones = []

for desplazamiento in lista_desplazamientos:

	puntos_grafica = []
	puntos_grafica.append(desplazamiento)
	

	for i in range(iteraciones):

		print (desplazamiento, end=" ")
		with open(archivo_resultados, 'a') as f:
			f.write(str(nro_simulacion+1)+", "+str(i+1)+", "+str(desplazamiento)+", ")
		realimentacion = simulacion_test(desplazamiento)
		desplazamiento += realimentacion
		puntos_grafica.append(desplazamiento)
		sys.stdout.flush()

	nro_simulacion=nro_simulacion+1
	print ()
	lista_resultados_simulaciones.append(puntos_grafica)

title = u'Simulación de desplazamiento con realimentación'
plt.figure(1)
plt.title(title)
plt.xlabel('Iteracion (n)')
plt.ylabel('Desplazamiento (m)')
grid()

cantidad_muestras = list (range(1,iteraciones+2))

i=0

for simulacion in lista_resultados_simulaciones:
	print (simulacion)
	plot(cantidad_muestras,simulacion,'-o',label='Sim '+str(i+1))
	plt.legend(loc='upper right')
	i+=1

plt.savefig('Simulacion'+''+'.png')
show()



subprocess.run(["octave",post_procesamiento, str(cantidad_columnas_csv), archivo_resultados])














# En la llamada a octave se le pasa el parámetro para la simulacion con un desplazamiento determinado.


# DOCUMENTACION BLOQUES
# -----------------------------------------------------------------------------
# 1) Generdor de canales.

# SCRIPT DE OCTAVE.
# Debe recibir el desplazamiento como argumento de la sig. forma:
# desplazamiento = str2num(arg{1});
# Debe escribir su salida en datos.c
# -----------------------------------------------------------------------------
#------------------------------------------------------------------------------
#
# serialized_text_data = subprocess.run(["./fft"], text=True, check=True, stdout=subprocess.PIPE).stdout 
#
# De esta forma no es necesario usar decode(), subprocess.run devuelve directamente la salida como texto,
# pero no es compatible con todas las versiones de python3, sino desde la 3.7 en adelante. Por lo tanto se
# el argumento "text" y el proceso devuelve codificado en Bytes la respuesta, por lo tanto es preciso
# usar una funcion que decodifique la cadena de bytes al tipo de datos string; funsion decode(). 
#
# serialized_text_data = subprocess.run(["./fft"], check=True, stdout=subprocess.PIPE).stdout
# serialized_text_data = serialized_text_data.decode()
#
#
