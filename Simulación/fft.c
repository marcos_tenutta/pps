// Compilar con gcc y linkear : -lfftw3 -lm.

#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <fftw3.h>
#include "datos.h" 			// N, vector, vector2.


int debug = 0;	
int encontrar_maximo(double*);	

// Bloques:

int	deteccion_de_fase();
int	comparador(int);
int	controlador(int);
 
int main()
{
	int desfase=0, posicion=0, correccion_desfase=0;
	
	desfase  = deteccion_de_fase();
	posicion = comparador(desfase);
	correccion_desfase = controlador(posicion);


	// Envía el dato a python 
	// por pipe de stdout.
	
			
	printf("%d, %d, %d", desfase, posicion, correccion_desfase);  
	
	return 0;
}

int deteccion_de_fase()
{

	// Se mide el desfase del canal 1 respecto al canal 0.
	// Canal 0 es el derecho, y el canal 1 es el izquierdo.
	// Canal 0  siempre está a la derecha del canal 1.
	
	// El desfase(1,0) (canal 1 respecto al canal 0) es negativo hacia la
	// izquierda y positivo hacia la derecha. El desfase siempre aumenta
	// a la derecha. Y el limite derecho del rango siempre es mayor
	// al limite izquierdo.
	
	
	int i= 0, desfase = 0;	
    double *channel0, *channel1, *conv_tiempo;
    fftw_complex *data0, *data1, *conv_frecuencia;
    fftw_plan plan0, plan1, plan0_inversa;
       
    channel0 = (double*) fftw_malloc(sizeof(double) * N);
    channel1 = (double*) fftw_malloc(sizeof(double) * N);
    
    data0 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N/2+1));
    data1 = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N/2+1));
    conv_frecuencia  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N/2+1));
    
    // Llenar vectores de entrada /////////////////////
    
    for (i=0;i<N;i++)
    {
		channel0[i] = vector[i];
	}
	
	    for (i=0;i<N;i++)
    {
		channel1[i] = vector2[N-i];  // Para que la convolución 
								    // sea una correlacion
	}

    //////////////////////////////////////////////////////////////

    plan0 = fftw_plan_dft_r2c_1d(N, channel0, data0, FFTW_ESTIMATE);
    plan1 = fftw_plan_dft_r2c_1d(N, channel1, data1, FFTW_ESTIMATE);
    

    fftw_execute(plan0); /* repeat as needed */
    fftw_execute(plan1); /* repeat as needed */
    
    
    
    //  Normalizar escala     ////////////////////////////////////
    
    
    for (i=0;i<N/2+1;i++)
    
    {
		 data0[i] = creal(data0[i])/(N/2) + cimag(data0[i])/(N/2) * I;
		}

    for (i=0;i<N/2+1;i++)
    {
		 data1[i] = creal(data1[i])/N + (cimag(data1[i])/N) * I;
		}
	
	////////////////////////////////////////////////////////////////////


	// Producto de complejos - Convoulución

	
    for (i=0;i<(N/2+1);i++)
    {
	    conv_frecuencia[i] = data0[i] * data1[i];
		}
		
		
	////////////////////////////////////////////////////////////////////
	


		
	if (debug==1)
	{
		printf("Resultado FFT - (%d muestras --- N/2+1, con N = %d)\n\n\n",N/2+1,N);
		
		for (i=0;i<(N/2+1);i++)
		{
			printf("Muestra %d:\t%.2f\t%.2f\n",i,creal(data0[i]),cimag(data0[i]));
			}
	
		// Respuesta en frecuencia - módulo
		
		printf("\nEspectro de frecuencias\n\n");
	
		for (i=0;i<(N/2+1);i++)
		{
			printf("Muestra %d:\t%.2f\n",i,cabs( data0[i]) );
			}
	
		//Convolución en dominio de la frecuencia
	
		printf("\n\nConvolución en el dominio de la frecuencia...\n\n");	
		
		for (i=0;i<(N/2+1);i++)
		{
			printf("Muestra %d:\t%.2f\t%.2f\n",i,creal(conv_frecuencia[i]),cimag(conv_frecuencia[i]));
		}
	}
	
	// FFT⁻¹ Transformada inversa
	
	conv_tiempo = (double*) fftw_malloc (sizeof(double) * 2*(N/2+1));
        
	plan0_inversa = fftw_plan_dft_c2r_1d(2*(N/2+1),conv_frecuencia,conv_tiempo, FFTW_ESTIMATE);
									

	fftw_execute(plan0_inversa);
	

	if (debug==1)
	{
		printf("\n\nConvolución en el dominio del tiempo...\n\n");	
    

		for (i=0;i<2*(N/2+1);i++)
		{
			printf("Muestra %d:\t%.2f\n",i,conv_tiempo[i]);
			}

		printf ("\n\n\n\tDesfase calculado: %d\t", desfase);
		printf ("Desfase simulado: %d\t", desplazamiento);
	}
	
		
	desfase = encontrar_maximo(conv_tiempo);

    fftw_destroy_plan(plan0);
    fftw_destroy_plan(plan1);
    fftw_destroy_plan(plan0_inversa);
    
    fftw_free(channel0);
    fftw_free(channel1);
    fftw_free(data0);
    fftw_free(data1);
    fftw_free(conv_tiempo);
	fftw_free(conv_frecuencia);
    fftw_cleanup();
    
    return desfase;
 }


int	comparador(int desfase)
{
	int posicion = 0;
	int desfase_deseado = 0; 
    int margen_posicion_deseada = 5;
	int limite_izquierdo_ok = desfase_deseado - margen_posicion_deseada;
	int limite_derecho_ok = desfase_deseado + margen_posicion_deseada;
	
	
	if (desfase >= limite_izquierdo_ok  &&  desfase <= limite_derecho_ok)
	{
		posicion = 0;
		}
		
	if (desfase < limite_izquierdo_ok)
	{
		posicion = 1;
		}
		
	if (desfase > limite_derecho_ok)
	{
		posicion = -1;
		}
	
	return posicion;
	}
	
	
	
int	controlador(int posicion)
{
	int correccion_desfase=0;
	
	
	if (posicion == -1)
	{
		correccion_desfase = 5;
		}
		
	if (posicion == 0)
	{
		correccion_desfase = 0;
		}
		
	if (posicion == -1)
	{
		correccion_desfase = -5;
		}
	
	
	return correccion_desfase;
	}

 
 
 int encontrar_maximo(double* vector)
{
	int i=0, pos=0;
	double mayor = 0;
			
    for ( i=0; i< (N/2+1) ; i++)
    {

        if (mayor < vector[i]) 	
        {							  
			mayor = vector[i];
            pos = i;
        }
     }
       
    return pos;
	}
	
	
int test_algoritmo_fft()
{
	int desfase_calculado = 0;
	int tolerancia = 2;	

	desfase_calculado  = deteccion_de_fase();

	if ( abs (abs(desplazamiento)- abs(desfase_calculado)) < tolerancia)
	{
		printf("----------   Test Passed!   ----------\n");
		}
	
	else
	{
		printf("----------   Test Failed!   ----------   ");
		printf ("\tDesfase calculado: %d\t", desfase_calculado);
		printf ("Desfase simulado: %d\n", desplazamiento);
		}

	return 0;
	
	}
 
// USAR VECTORES DE TAMAÑO POTENCIA DE DOS grandes. (512,1024)

// El vector que devuelve va de 0 (continua) a N/2; 

// Resolución de la DFT:
// 48000 / 32
// f = (0: 1/32: 32)*48000



// En complex.h:

// cabs(double complex); Calcular valor absoluto de cun complejo tipo double.
// Para el producto existe la sobrecarga de operadores, así que todo bien jeje.
// Si los operandos de * son complex (float, double, etc) pueden multiplicares directamente,
// y el resultado debe guradarse en otro complejo igual.



//			FFT - FFTW-1

// Input and output arrays are of different sizes and types: the input 
// is n real numbers, while the output is n/2+1 complex numbers 
// this also requires slight “padding” of the input array.

// The inverse transform (complex to real) has the side-effect of 
// overwriting its input array, by default.

//----------------------------------------------------------------------

// fftw_plan fftw_plan_dft_r2c_1d(int n, double *in, fftw_complex *out,
                               //unsigned flags);
// fftw_plan fftw_plan_dft_c2r_1d(int n, fftw_complex *in, double *out,
                               //unsigned flags);

// n is the “logical” size of the DFT, not necessarily the physical size
// of the array. In particular, the real (double) array has n elements,
// while the complex (fftw_complex) array has n/2+1 elements 
// (where the division is rounded down).

// As noted above, the c2r transform destroys its input array even for 
// out-of-place transforms. This can be prevented, if necessary, 
// by including FFTW_PRESERVE_INPUT in the flags, with unfortunately
// some sacrifice in performance
