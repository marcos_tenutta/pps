pkg load io

arg = argv(); 

cantidad_columnas = str2num(arg{1});
filename = arg{2};

nombres_columnas = textread (filename, '%s', cantidad_columnas, "delimiter",",");

# Se debe transponer por la forma en que se pasa el vector a la funcion que
# Genera el excel

nombres_columnas = nombres_columnas';
  
data = dlmread ("resultados.csv", ",", 1, 0);


# data = dlmread (file, sep, r0, c0)
#
# Lee el archivo resultados.csv, separado por comas, desde
# la segunda fila, la primera es la numero 0. 
# (por lo que omite la descripcion)
# y desde la primera columna



# Para generar excel es necesario instalar un paquete: 
# Se debe descargar de "https://octave.sourceforge.io/io/"
# Una vez octave esta ubicado en el directorio, se debe hacer:

# >>> pkg install io-2.6.1

# En el script, se debe cargar antes de usar una funcion del paquete:

# pkg load io

rstatus = xlswrite ("resultados.xlsx", nombres_columnas);
rstatus = xlswrite ("resultados.xlsx", data, "A2:IV65336");
