`timescale 1us / 1ps

// IMPORTANTE!! EL ADAPTADOR ENTRE 5V y 3.3V INVIERTE LA LOGICA.
// Por lo que salida final del trigger se niega, y el echo se describe con la
// lógica inversa a la real.

module sensor_ultrasonico
#(
//   parameter COUNT_60_ms = 6000000,        // 100 Mhz
//   parameter TRIGGER_COUNT_10_us = 1000,   // 100 Mhz
	  parameter TRIGGER_COUNT_10_us = 500,    // 50  Mhz
	  parameter COUNT_60_ms = 6000000,        // 50  Mhz
	  parameter COUNT_1_s = 50000000          // 50  Mhz
)
(
    input clock,
    input reset,
    input echo,
    output trigger,
    output ready,
    output [9:0] data
    );
	 
	
	reg [25:0] main_counter;
	reg [21:0] echo_count;
//	reg [27:0] aux_data;   //Usado a 100 MHz, debe ser hasta 64  veces más grande que echo_count. (en realidad x45)
	reg [28:0] aux_data;	  //Usado a  50 MHz, debe ser hasta 128 veces más grande que echo_count. (en realidad x90)
	reg aux_trigger; 
	reg aux_ready;

/////////////////////////////////////////////////////
// Contador principal, determina el ciclo de medicion

	always @(posedge clock) begin
	
		if (reset)				
			main_counter <= 26'b0;
		else begin
			if (main_counter <= COUNT_60_ms )	
				 main_counter <= main_counter + 1;
			else
				 main_counter <= 26'b0;
		end	
	end
	
/////////////////////////////////////////////////////
// Sincronizador de entrada de control externa: echo.
// Con detector de flanco

	reg signal_echo;
	reg aux_echo_old;
	reg aux_echo;
	wire positive_edge_echo;
	
	always @(posedge clock) begin
		if (reset) begin
			  signal_echo  <= 1'b0;
			  aux_echo_old <= 1'b0;
			  aux_echo     <= 1'b0;
	   end
		else begin
			  signal_echo  <= echo;
			  aux_echo_old <= signal_echo;
			  aux_echo     <= aux_echo_old;
		end		 
	end
	
	assign positive_edge_echo = ~aux_echo & aux_echo_old;

/////////////////////////////////////////////////////
// FlipFlop toggle, generar señal ready

   always @(posedge clock) begin
      if (aux_trigger)
         aux_ready <= 1'b0;
		else begin
			if (positive_edge_echo)
				aux_ready <= ~aux_ready;
			else
				aux_ready <=  aux_ready;
      end
	end

/////////////////////////////////////////////////////
// Controlador: generacion de trigger y cuenta de echo.
	
	always @(posedge clock) begin

		if (reset) begin		
			aux_trigger <= 1'b0;
			echo_count  <= 22'b0; 	
		end
		
		else begin
		
			if (main_counter <= TRIGGER_COUNT_10_us) begin
				aux_trigger <= 1'b1;
				echo_count <= 22'b0; 
			end	
			
			else if (main_counter > TRIGGER_COUNT_10_us) begin	
			
					aux_trigger <= 0;			
				
					if (~aux_echo) begin
						echo_count <= echo_count + 1;
					end 
					else
						echo_count <= echo_count;
			end			
		end
    		
	end

////////////////////////////////////////////////////////////////////
// Salida  - El cálculo depende de la velocidad del clock.

		
// 100 Mhz : Distancia [cm] = Cuenta/5831. 
// Para aproximar, se divide por 5825.42:				
// Primero se multiplica por 45, Luego se divide por 262144 (2^18), tomando los bits más significativos.		
		
// 50 Mhz : Es exactamente el doble que el cálculo a 100Mhz. 
// Distancia [cm] = (Cuenta/5831) x 2.		
// Cuenta x 90 /26214. En este caso es necesario agrandar el ancho del bus, para que no se exceda la multiplicacion.
		
	always @(*) begin
	
		if (reset)	
//		   aux_data 	<= 28'b0;              //A 100 MHZ
			aux_data 	<= 29'b0;              //A 50 Mhz
		else
//	   	aux_data <= echo_count*(6'b101101 );    //A 100 MHZ
			aux_data <= echo_count*(7'b1011010);    //A 50 Mhz
	end
	
	assign data[9:0] = aux_data[27:18];        //A 50 Mhz
	assign trigger = ~aux_trigger;	
	assign ready = aux_ready;	
		
endmodule
